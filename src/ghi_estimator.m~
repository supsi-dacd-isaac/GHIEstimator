function [GHI,y_hat,last_score] = ghi_estimator(PV,GHI_obs,t,location,W,Ta,n_init,opt_pars,derivatives,k_outlier,ftrust,plant_weights)
% Estimate GHI iteratively, starting from a first guess done through a grid
% search. The function take as input the estimated orientations and nominal
% powers of the PV fields that generated the power observations. These
% estimation are provided by means of a coefficient matrix W.
% Inputs: PV: T*N matrix of PV plants power observations, where T is the 
%             total number of observations and N is the number of PV power
%             plants.
%         GHI_obs: 

addpath(genpath('../irradianceTransformations/'));
addpath('../PV_LIB Version 1_32 Development/');

if nargin<9
    derivatives = false;
end

if nargin<10
    k_outlier = false;
end

if nargin<11
    ftrust = [];
end

if nargin<12
    plant_weights = ones(1,size(PV,2));
end
% if ~isfield(opt_pars,'correction')
%     opt_pars.correction = 'T';
% end

if isempty(GHI_obs)
    GHI_obs = zeros(size(PV,1),1);
end
correction = opt_pars.correction;
%% Define additional parameters
[sbplt,]=find_plot_layout(size(PV,2));
equallySpaced = true;
n_obs = size(PV,1);
n_pv = size(PV,2);
% define weigths for the PVs
if ~isempty(ftrust)
    Time = pvl_maketimestruct(t, ones(size(t))*location.UTC); %generate a structured Time
    Location = pvl_makelocationstruct(location.latitude,location.longitude,location.altitude); %Generate a structured Location
    [SunAz, SunEl, ~, ~] = pvl_ephemeris(Time,Location); %compute azimuth, elevation (90�-zenit) , apparent elevation and solar time. NB:can take additional arguments    
    for i=1:size(PV,2)
        sun_err(:,i) = ftrust{i}(SunAz,SunEl);
    end
    
    if opt_pars.expWeight
        sun_dist = exp(-abs(sun_err)); % must be abs or ^2 of the rerr o be sign invariant. Do not use 1-abs(rerr) because abs(rerr) can be > 1
    else
        sun_dist = min(100/3,1./abs(sun_err)); % must be abs or ^2 of the rerr o be sign invariant. Do not use 1-abs(rerr) because abs(rerr) can be > 1
    end
    sun_norm = sum(sun_dist,2)/size(PV,2);	% if not trust function sum(sun_weights) = n_pv. To let this unchanged normalize for the sum
    sun_weights = sun_dist./repmat(sun_norm,1,size(PV,2));
    % plot sun weights
    res=10; 
    ix1=round(SunAz/res)+1;
    ix2=round(SunEl/res)+1;
    
    [X,Y] = meshgrid(0:res:360,0:res:90);
    for i=1:size(PV,2)
        filter = SunEl>0 & isfinite(sun_weights(:,i));
        E(:,:,i) = accumarray([ix1(filter),ix2(filter)],sun_weights(filter,i),[360/res+1 90/res+1],@(x) quantile(x,0.05),nan);
        figure; 
        surfc(X,Y,E(:,:,i)','LineStyle','none');colorbar;
        caxis([0,2]);
    end
    drawnow
end

if isempty(ftrust)
    weights = plant_weights.*(1/n_pv);
else
    weights = plant_weights.*(1/n_pv).*sun_weights;
end


ghi_max_mult = 1.1;
max_iter = opt_pars.max_iter;
lambda  = repmat(opt_pars.lambda,n_obs,1);
do_plots = opt_pars.do_plots;
if do_plots
    for i=1:3
        h(i) = figure;
    end
end
%% Create matrix of initial GHI evaluations
GHI_clear = ghi_clear_sky(t,location,location.UTC);
GHI_mat = repmat(GHI_clear,1,n_init).*(repmat([linspace(0.001,ghi_max_mult,n_init)],n_obs,1));

%% Find a good starting point for the solution of the non-convex problem
wb=waitbar(0,'grid search...');
for i=1:n_init
    waitbar(i/n_init,wb,sprintf('grid search... (%.0f/%.0f)',i,n_init));
    proxies = compute_proxies(GHI_mat(:,i),t,Ta,location,equallySpaced,correction);
    y_hat = proxies*W;
    err_iter = PV - y_hat;
    if isempty(ftrust)
        err_grad(:,i) = err_iter*weights';
    else
        err_grad(:,i) = sum(err_iter.*weights,2);
    end
end
close(wb);
[~,col_idx] = min(abs(err_grad),[],2);
abs_ind = sub2ind(size(err_grad),[1:n_obs]',col_idx);
GHI = GHI_mat(abs_ind);
proxies = compute_proxies(GHI,t,Ta,location,equallySpaced,correction);
y_hat = proxies*W;
rrmse = mean(mean(((PV -y_hat)).^2).^0.5);
rrmse(2) = rrmse;
% Plot first guess results
figure; plot(PV); hold on; plot(col_idx*100);
plot(GHI*10,'LineWidth',2);
plot(GHI_obs*10,'k','LineWidth',2);
for i=1:size(PV,2)
    labels{i} = strcat('PV_',num2str(i));
end
labels_1 = labels;
labels_1{length(labels_1)+1} = 'min idx';
labels_1{length(labels_1)+1} = 'GHI estimated';
labels_1{length(labels_1)+1} = 'GHI observed';
legend(labels_1)

%% Backprop from starting point
n_iter=1;
delta_ghi = 1e-3;
is_outlier = false(size(PV));
outlier_kickoff = [10,20,30];
% err_iter_old = -(PV - y_hat);
while n_iter < max_iter &&  rrmse(n_iter)> 1e-4 
    
    proxies = compute_proxies(GHI,t,Ta,location,equallySpaced,correction);
    % 
    y_hat = proxies*W;
    
    if k_outlier>0 && any(n_iter==outlier_kickoff)
        is_outlier = detect_outliers(PV,y_hat,k_outlier);
    end
    if derivatives
        % Numerically compute the derivatives of the objective function 
        % with respect to the GHI signal
        proxies_up = compute_proxies(GHI+delta_ghi,t,Ta,location,equallySpaced,correction);
        y_hat_up = proxies_up*W;
        y_der = (y_hat_up-y_hat)/delta_ghi;
        err_iter = -(PV - y_hat);
        err_iter(is_outlier) = 0;
        err_grad = mean(err_iter.*y_der.*weights,2);
        
        % Perform a gradient descent step
        GHI1 = GHI -err_grad.*lambda;
        
        % exclude non-physical values (keep trak of them and put relative observations to 0)
        do_not_take = (GHI1<0);
        GHI1(GHI1<0) = 0;
        
        % compute again the error
        proxies = compute_proxies(GHI1,t,Ta,location,equallySpaced,correction);
        y_hat_1 = proxies*W;
        err_iter1 = -(PV - y_hat_1);
        err_iter1(is_outlier) = 0;
        do_train = mean(abs(err_iter1.*weights)-abs(err_iter.*weights),2)<0;
        
        % Update GHI estimation for those observations whose PV error is
        % decreasing
        GHI(do_train) = GHI1(do_train);
        
        % lambda decay: rescale lambda for those observations whose PV 
        % error is non-decreasing 
        lambda(~do_train | do_not_take) = lambda(~do_train | do_not_take)*0.9;
    else
        err_iter = PV - y_hat;
        err_iter(is_outlier) = 0;
        if isempty(ftrust)
            err_grad = err_iter*weights';
        else
            err_grad = sum(err_iter.*weights,2);
        end
        do_train = 1;
        GHI = GHI + err_grad.*lambda*quantile(GHI_clear,0.9);
    end
    GHI(GHI<0) = 0;
    rrmse(n_iter+1) = mean(mean(((PV -y_hat)).^2).^0.5);
    score(n_iter) = (mean((GHI-GHI_obs).^2)^0.5)/mean(GHI_obs);
    % Plot current solution
    if do_plots
        figure(h(1))
        subplot(2,2,[1,2])
        plot(PV);hold on;
        plot(y_hat,'--');
        title(sprintf('mean RRMSE:%0.2e',rrmse(end)))
        labels_2 = labels;
        for i=1:size(PV,2)
            labels_2{length(labels_2)+1} = sprintf('PV_{%i,est}',num2str(i));
        end
        legend(labels_2)
        hold off;
        
        subplot 223
        semilogy(rrmse(2:end),'r.','markersize',15);
        title('rrmse')
        subplot 224
        semilogy(score,'r.','markersize',15);
        title('sum of squared errors')
        figure(h(2))
        for i=1:size(PV,2)
            subplot(sbplt(1),sbplt(2),i)
            scatter(PV(:,i),y_hat(:,i),'.');
        end
        
        figure(h(3))
        plot_idx = 1:min(1000,length(PV));
        plot(PV(plot_idx,:));hold on;
        plot(y_hat(plot_idx,:),'--');
        plot(GHI(plot_idx));
        labels_3 = labels_2;
        labels_3{length(labels_3)+1} = 'GHI_est';
        legend(labels_3)
        hold off;
        drawnow;
    end
    % fprintf('\nIteration %i, RRMSE %0.2e, SCORE %0.2e',[n_iter,rrmse(n_iter),score(n_iter)])
    fprintf('\nIteration %i, RRMSE %0.2e, mean(lambda): %0.2e, score = %0.2e',[n_iter,rrmse(n_iter),mean(lambda), score(end)])
    fprintf(' Outliers: %i',sum(is_outlier(:)));
    n_iter = n_iter +1;
end
last_score = score(end);

end

function is_outlier = detect_outliers(PV,y_hat,k)

n_pv = size(PV,2);
pe = ((y_hat-PV))./PV;
pe_q025 = repmat(quantile(pe,0.25,2),1,n_pv);
pe_q050 = repmat(quantile(pe,0.5,2),1,n_pv);
pe_q075 = repmat(quantile(pe,0.75,2),1,n_pv);
sample_mean = mean(pe,2)-min(pe,[],2);
sample_median = median(pe,2)-min(pe,[],2);
figure; plot(pe); hold on;
pe_IQ = pe_q075 - pe_q025;
if size(PV,2)<5
k_ind = abs((sample_mean-sample_median));
k_ind = k_ind./max([abs(sample_mean),abs(sample_mean)],[],2);
% plot(k_ind,'.','markersize',20);
k = repmat(k,size(pe,1),1).*(1-k_ind/1.5);
end

for i=1:size(PV,2)
    %     is_outlier(:,i) = pe(:,i) > k*pe_q050(:,i);
    is_outlier(:,i) = pe(:,i) < pe_q025(:,i) - k.*pe_IQ(:,i) | pe(:,i) > pe_q075(:,i) + k.*pe_IQ(:,i);
end

end
